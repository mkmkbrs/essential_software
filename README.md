# Essential software

The list of essential and minimalistic software that I use on the daily basis, plus some extra stuff.

## Essentials

| Category | Name | Description | Interface |
|----------|------|-------------|-----------|
| Shell    | bash | The only shell you'll ever need | CLI |
| Window Manager | [dwm](https://dwm.suckless.org/) | A fast and extensible dynamic window manager written in C | GUI |
| Terminal emulator | [rxvt-unicode](https://github.com/exg/rxvt-unicode) | A customizable terminal emulator | CLI |
| Text Editor | [vim](https://github.com/vim/vim), [emacs](https://www.gnu.org/software/emacs/) + [doom](https://github.com/doomemacs/doomemacs) | You know them, you love them | TUI, GUI |
| File Manager | [vifm](https://github.com/vifm/vifm) | The best file manager for the vim user | TUI |
| Browser | [ungoogled-chromium](https://github.com/ungoogled-software/ungoogled-chromium), [links](http://links.twibright.com/download.php) | Use whatever for general purpose since they all suck the same. `links` is good for a quick search | GUI, TUI |
| Program launcher | [dmenu](https://tools.suckless.org/dmenu/) | A dynamic menu able to interface with shell scripts | GUI |
| Image viewer | [sxiv](https://github.com/muennich/sxiv), [feh](https://github.com/derf/feh) | Two of them | GUI |
| Video player | [mpv](https://github.com/mpv-player/mpv)/[omxplayer (Raspberry Pi)](https://github.com/popcornmix/omxplayer/) | `omxplayer` for Raspberry Pi provides much smoother playback than MPV or VLC out of the box | GUI |
| Music player | [cmus](https://github.com/cmus/cmus) | Simple and functional music player | TUI & CLI |
| PDF reader | [zathura](https://github.com/pwmt/zathura) | A minimal PDF reader with vim-centric keybindings | GUI |
| RSS reader | [newsboat](https://github.com/newsboat/newsboat)/[elfeed](https://github.com/skeeto/elfeed) | THE RSS reader | TUI, GUI |
| Email client | [neomutt](https://github.com/neomutt/neomutt) | Highly configurable email client with extensive documentation, available everywhere | TUI |
| Backup tools | [rsync](https://github.com/WayneD/rsync) | A good pair for `sftp` | CLI |
| Remote access | [sshfs](https://github.com/libfuse/sshfs) | Mount remote filesystems and operate on them with a file manager of your choice | None |
| Firewall | ufw | Dead simple firewall | CLI |
| Notification daemon | [dunst](https://github.com/dunst-project/dunst) | Minimal notification daemon that doesn't gets in your way | CLI |
| Keyboard layout | [xkblayout-state](https://github.com/nonpop/xkblayout-state) | A small program to get/set the current keyboard layout | CLI |
| Process monitor| [htop](https://htop.dev/) | A simple process monitor, available everywhere | TUI |
| Disk manager | [udisks2](https://www.freedesktop.org/wiki/Software/udisks/) | Provides a daemon and a command line tool to manipulate storage devices connected via USB | CLI |

## Extras

| Category | Name | Description | Interface |
|----------|------|-------------|-----------|
| Dwm extentions | [dwmblocks](https://github.com/torrinfail/dwmblocks) | A pretty much essential extra program to run multiple scripts for the `dwm` bar on multiple independent timers | CLI |
| Fonts | [noto-fonts](https://github.com/googlefonts/noto-fonts), [siji](https://github.com/stark/siji) | `siji` is a glyph font that provides a set of icons for your `dwm` bar | None |
| Compositor | [picom](https://github.com/yshui/picom) | For window transparency effects | CLI |
| Other | [dupeguru](dupeguru.voltaicideas.net/) | A neat tool to search for duplicate files, music and images | GUI |
| Terminal colorscheme | [pywal](https://github.com/dylanaraps/pywal) | Generates a terminal color scheme based on an image or wallpaper | CLI |
| Youtube | [yt-dlp](https://github.com/yt-dlp/yt-dlp/) | Download any video or audio from YouTube | CLI |
| Pulseaudio | [pulsemixer](https://github.com/GeorgeFilipkin/pulsemixer), [pamixer](https://github.com/cdemoulins/pamixer) | `pulsemixer` is a visual audio mixer. `pamixer` is a CLI alternative with better responsiveness, hence better for scripts | TUI & CLI |
| Tool | [unclutter](https://github.com/Airblader/unclutter-xfixes) | Automatically hides mouse cursor from the screen when you don't need it | CLI |
| Tool | [scrot](https://github.com/dreamer/scrot) | The simplest screenshot tool there is | CLI |
| Tool | [ncdu](https://dev.yorhel.nl/ncdu) | A disk usage analyzer with an ncurses interface | TUI |
